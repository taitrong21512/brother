# AMG.Brother.Application
This layer contains all application logic. It is dependent on the  AMG.Brother.DAL, AMG.Brother.Infrastructure layer, but has no dependencies on any other layer or project.